shoelaces (1.3.2+ds-1) unstable; urgency=medium

  * New upstream version 1.3.2+ds.
  * Bump standards version to 4.6.2 (no changes required).
  * Add lintian overrides for spelling errors octects, whE and wTH.
    They are false positives. The strings are not included in the source.
  * d/control: Update maintainer email address.
  * d/copyright: Update copyright years.
  * d/rules: Drop custom build parameters to rely on dh_golang defaults.
  * d/watch: Do not use filenamemangle.
    Lintian recommends to instead use USCAN_SYMLINK=rename.

 -- Raúl Benencia <rul@debian.org>  Fri, 09 Jun 2023 10:07:34 -0700

shoelaces (1.2.0+ds-1) unstable; urgency=medium

  * New upstream version 1.2.0+ds.
  * Repack through uscan.
  * Bump to Standards-Version 4.5.1. No changes required.
  * Remove deprecated port parameter on shoelaces.default.

 -- Raúl Benencia <rul@kalgan.cc>  Sat, 10 Apr 2021 15:51:02 -0700

shoelaces (1.1.0+ds-1) unstable; urgency=medium

  * New upstream release.
  * Fix bootstrap symlinks.
  * Depend on debhelper-compat.
  * Bump debhelper compat.
  * Bump to Standards-Version 4.5.0. No changes required.
  * Add Bug-Database and Bug-Submit upstream metadata.
  * Remove -guest from VCS control fields.

 -- Raúl Benencia <rul@kalgan.cc>  Sat, 09 May 2020 10:28:01 -0700

shoelaces (1.0.2-2) unstable; urgency=medium

  * Enable sid transition.

 -- Raúl Benencia <rul@kalgan.cc>  Sun, 03 May 2020 01:21:38 -0300

shoelaces (1.0.2-1) experimental; urgency=medium

  * Initial release (Closes: #905723)

 -- Raúl Benencia <rul@kalgan.cc>  Sat, 27 Jul 2019 17:18:37 -0300
